;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2016-2022 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2022 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web controller)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web request)
  #:use-module (web uri)
  #:use-module (webutils sessions)
  #:use-module (gcrypt base64)
  #:use-module (gcrypt mac)
  #:use-module (mumi config)
  #:use-module ((mumi debbugs)
                #:select (bug-status bug-archived bug-subject bug-done))
  #:use-module (mumi jobs)
  #:use-module (mumi messages)
  #:use-module (mumi web render)
  #:use-module (mumi web download)
  #:use-module (mumi web graphql)
  #:use-module (mumi web util)
  #:use-module (mumi web view html)
  #:use-module ((mumi xapian) #:select (search))
  #:export (controller))

(define-syntax-rule (-> target functions ...)
  (fold (lambda (f val) (and=> val f))
        target
        (list functions ...)))

(define (%session-manager)
  (let ((key-file (string-append (%config 'key-dir) "/signing-key")))
    (unless (file-exists? key-file)
      (with-output-to-file key-file
        (lambda () (write (base64-encode (generate-signing-key))))))
    (make-session-manager
     (with-input-from-file key-file read)
     ;; expire session after 30 mins
     #:expire-delta '(0 0 30))))

(define (controller request body)
  (match-lambda
    (('GET)
     (render-html (index)
                  #:extra-headers
                  '((cache-control . ((max-age . 60))))))
    (('GET "easy")
     (render-html (list-of-matching-bugs "tag:easy" (easy-bugs))))
    (('GET "recent")
     (render-html (list-of-recent-issues)))
    (('GET "forgotten")
     (render-html (list-of-forgotten-issues)))
    (('GET "wishlist")
     (render-html
      (list-of-matching-bugs "severity:wishlist is:open"
                             (bugs-by-severity "wishlist" "open"))))
    (((or 'GET 'POST) "graphql")
     (handle-graphql request body))
    (('GET "search")
     (let ((query (-> request
                      request-uri
                      uri-query
                      parse-query-string
                      (cut assoc-ref <> "query"))))
       (cond
        ;; TODO: query should not be empty!
        ((or (not query)
             (string-null? (string-trim query)))
         (redirect '()))

        ;; For convenience
        ((string-prefix? "id:" query) =>
         (lambda _ (redirect (list "issue" (string-drop query (string-length "id:"))))))
        ((string-prefix? "#" query) =>
         (lambda _ (redirect (list "issue" (string-drop query (string-length "#"))))))
        ((string->number query) =>
         (lambda _ (redirect (list "issue" query))))

        ;; Search for matching messages and return list of bug reports
        ;; that belong to them.
        (else
         (render-html
          (list-of-matching-bugs query
                                 (search-bugs (string-join
                                               (process-query query)))))))))
    ((or ('GET "issue" (? string->number id))
         ('GET (? string->number id)))
     (let ((bug (bug-status id))
           (plain? (-> request
                       request-uri
                       uri-query
                       parse-query-string
                       (cut assoc-ref <> "plain")))
           (message (match (uri-query (request-uri request))
                      ("comment-ok"
                       '(info . "Your comment has been submitted!"))
                      ("comment-error"
                       '(error . "There was an error submitting your comment!"))
                      (_ #f))))
       (if bug
           ;; Record the current issue id in an encrypted cookie.
           ;; This will be verified when posting a comment.
           (let* ((cookie-header
                   (set-session (%session-manager) `((issue-id . ,id))))
                  (headers
                   (cond
                    ((bug-archived bug)
                     ;; Tell browser to cache this for 12 hours.
                     (cons cookie-header
                           '((cache-control . ((max-age . 43200))))))
                    ((bug-done bug)
                     ;; Tell browser to cache this for 1 hour.
                     (cons cookie-header
                           '((cache-control . ((max-age . 3600))))))
                    (else (list cookie-header))))
                  (page (issue-page bug
                                    #:flash-message message
                                    #:plain? plain?)))
             (if page
                 (render-html page #:extra-headers headers)
                 (render-html (unknown id))))
           (render-html (unknown id)))))
    (('GET "msgid" msgid)
     (match (search (format #false "msgid:~a" msgid))
       ((id . rest)
        (redirect (list "issue" id)
                  #:fragment (format #false "msgid-~a" (msgid-hash msgid))))
       (_ (render-html (unknown msgid)))))
    (('POST "issue" (? string->number id) "comment")
     (if (mailer-enabled?)
         (let ((headers   (request-headers request))
               (form-data (parse-form-submission request body))
               (cookie    (or (session-data (%session-manager) request)
                              '()))
               (bug       (bug-status id)))
           (if (and
                bug
                ;; The encrypted cookie must be fresh and contain the
                ;; current issue id.
                (and=> (assoc-ref cookie 'issue-id)
                       (cut string=? id <>))
                ;; The honeypot field "validation" must remain empty
                (let ((val (assoc-ref form-data 'validation)))
                  (and val (string-null? (string-trim-both val))))
                ;; Submission may not have happened too quickly
                (let ((time (assoc-ref form-data 'timestamp)))
                  (and time (reasonable-timestamp? time)))
                ;; Message must not be too short
                (and=> (assoc-ref form-data 'text)
                       (lambda (text)
                         (> (string-length (string-trim-both text)) 10)))
                ;; Message must have sender
                (and=> (assoc-ref form-data 'from)
                       (compose (negate string-null?) string-trim-both)))
               (begin
                 ;; Send comment to list
                 (enqueue 'mail
                          `((from . ,(string-trim-both (assoc-ref form-data 'from)))
                            (subject . ,(bug-subject bug))
                            (to . ,(format #f "~a@~a"
                                           id (%config 'debbugs-domain)))
                            (text . ,(assoc-ref form-data 'text))))
                 (redirect (list "issue" id) #:query "comment-ok"))
               (redirect (list "issue" id) #:query "comment-error")))
         (redirect (list "issue" id) #:query "comment-error")))
    (('GET "issue" (? string->number id)
           "attachment" (? string->number msg-num)
           (? string->number path) ...)
     (handle-download (string->number id)
                      (string->number msg-num)
                      (map string->number path)))
    (('GET "issue" (? string->number id)
           "patch-set" . patch-set-num)
     (let* ((patch-set-num* (match patch-set-num
                              (() #false)
                              ((f . rest) f)))
            (content (patch-messages id patch-set-num*)))
       (if content
           (list `((content-type text)
                   (content-disposition
                    text/plain
                    (filename
                     . ,(format #f "~a-patch-set~:[-~a~;~].mbox"
                                id patch-set-num* patch-set-num*))))
                 content)
           (not-found (request-uri request)))))
    (('GET "issue" (? string->number id)
           "raw" (? string->number msg-num))
     (download-raw (string->number id)
                   (string->number msg-num)))
    (('GET "issue" not-an-id)
     (render-html (unknown not-an-id)))
    (('GET "help")
     (render-html (help)
                  ;; Cache for 24 hours.
                  #:extra-headers
                  '((cache-control . ((max-age . 86400))))))
    (('GET path ...)
     (render-static-asset request))))
