;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2016, 2017, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2014  David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web util)
  #:use-module (mumi config)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-26)
  #:use-module (rnrs bytevectors)
  #:use-module (gcrypt hmac)
  #:use-module (gcrypt base64)
  #:use-module (gcrypt base16)
  #:use-module (gcrypt hash)
  #:use-module (srfi srfi-1)
  #:use-module (web request)
  #:use-module (web uri)
  #:export (parse-query-string
            request-path-components
            file-extension
            directory?

            msgid-hash
            timestamp!
            reasonable-timestamp?))

(define (parse-query-string query)
  "Parse and decode the URI query string QUERY and return an alist."
  (let ((pairs (string-split query #\&)))
    (fold (lambda (pair acc)
            (match (map uri-decode (string-split pair #\=))
              ((key) (cons (cons key "true") acc))
              ((key value) (cons (cons key value) acc))))
          '() pairs)))

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define (file-extension file-name)
  (last (string-split file-name #\.)))

(define (directory? filename)
  (string=? filename (dirname filename)))

(define (msgid-hash s)
  (bytevector->base16-string (sha1 (string->utf8 s))))

(define (timestamp!)
  (let* ((seconds (current-time))
         (key-file (string-append (%config 'key-dir) "/signing-key"))
         (key (with-input-from-file key-file read))
         (sig (sign-data-base64 key
                                (call-with-output-string
                                  (lambda (port)
                                    (write seconds port)))
                                #:algorithm 'sha512)))
    (base64-encode
     (string->utf8 (format #f "~a$~a" seconds sig)))))

(define* (reasonable-timestamp? data #:optional (min 5) (max (* 60 10)))
  "Validate the signature in DATA, extract the timestamp, and return
#T if it is no older than MAX seconds and no more recent than MIN
seconds."
  (let* ((now (current-time))
         (key-file (string-append (%config 'key-dir) "/signing-key"))
         (key (with-input-from-file key-file read)))
    (match (string-split (utf8->string (base64-decode data)) #\$)
      ((decoded sig)
       (and (verify-sig-base64 key
                               (string->utf8 decoded) sig
                               #:algorithm 'sha512)
            (and=> (string->number decoded)
                   (lambda (then)
                     (let ((diff (- now then)))
                       (and (> diff min) (< diff max)))))))
      (_ #f))))
