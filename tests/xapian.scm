;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (test-xapian)
  #:use-module (mumi xapian)
  #:use-module (mumi test-utils)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 match))

(test-begin "xapian")

(define (time->datestamp time)
  (date->string (time-utc->date time) "~Y~m~d"))

(define sanitize
  (@@ (mumi xapian) sanitize-date-range))

(test-equal "sanitize-date-range: turns \"today\" into datestamp"
  (sanitize "date:today..today")
  (let ((today (time->datestamp (current-time))))
    (format #f "date:~a..~a" today today)))

(test-equal "sanitize-date-range: turns \"now\" into datestamp"
  (sanitize "date:today..now")
  (let ((today (time->datestamp (current-time))))
    (format #f "date:~a..~a" today today)))

(test-equal "sanitize-date-range: turns \"yesterday\" into datestamp"
  (sanitize "date:yesterday..today")
  (let* ((today (current-time))
         (yesterday (subtract-duration today (make-time time-duration 0 (* 60 60 24)))))
    (format #f "date:~a..~a"
            (time->datestamp yesterday)
            (time->datestamp today))))

(test-equal "sanitize-date-range: pads short dates"
  (sanitize "date:20..19")
  "date:20000000..19000000")

(test-equal "sanitize-date-range: turns 1m into datestamp one month ago"
  (sanitize "date:1m..now")
  (let* ((today (current-time))
         (1m (subtract-duration today (make-time time-duration 0 (* 60 60 24 30)))))
    (format #f "date:~a..~a"
            (time->datestamp 1m)
            (time->datestamp today))))

(define tokenize
  (@@ (mumi xapian) tokenize))

(test-equal "tokenize: keeps phrases intact 1"
  (tokenize "subject:\"hello world\" how are you")
  '("subject:\"hello world\"" "how" "are" "you"))

(test-equal "tokenize: keeps phrases intact 2"
  (tokenize "subject:\"hello world\" how \"are\" you")
  '("subject:\"hello world\"" "how" "\"are\"" "you"))

(define data-dir
  (string-append (getenv "abs_top_srcdir") "/tests/data"))
(define db-dir (tmpnam))
(mkdir db-dir)

(mock ((mumi config) %config
       (match-lambda
         ('cache-ttl 10)
         ('db-dir db-dir)
         ('data-dir data-dir)
         ('packages '("guix"))))
      (index! #:full? #t)
      (dynamic-wind
        (lambda () #t)
        (lambda ()
          (test-assert "search: finds simple strings"
            (let ((result (search "hello" #:pagesize 2)))
              (and (= 2 (length result))
                   (member "33299" result)
                   (member "47187" result))))
          (test-equal "search: supports submitter prefix with partial name"
            '("26095")
            (search "submitter:Ricardo" #:pagesize 2))
          (test-equal "search: supports submitter prefix with partial email address"
            '("26095")
            (search "submitter:rekado" #:pagesize 2))
          (test-equal "search: supports submitter prefix with phrase"
            '("26095")
            (search "submitter:\"Ricardo Wurmus\"" #:pagesize 2))
          (test-assert "search: supports author prefix with email address"
            (let ((result (search "author:ludo" #:pagesize 2)))
              (and (= 2 (length result))
                   (member "26095" result)
                   (member "33299" result))))
          (test-equal "search: finds by message id"
            '("33299")
            (search "msgid:c78be403-0616-67a0-fd5a-e1196b6a14d1@example.com"
                    #:pagesize 2))
          (test-equal "search: finds by long message id"
            '("47187")
            (search "msgid:X0AJfvGmJvZOXkqcxiL1wDpQGbPYwaMG5V24ltJiXsvMhc8i8OZkWd_uAf18tMpgcSq1izVJTiurVFRaflG2_dOtTi7UzrOZwT9DcV0gFo0=@protonmail.com"
                    #:pagesize 2)))
        (lambda ()
          (delete-file-recursively db-dir))))

(test-end "xapian")
