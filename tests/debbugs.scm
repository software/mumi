;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (test-debbugs)
  #:use-module (mumi config)
  #:use-module (mumi debbugs)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-64))

(define-syntax-rule (mock (module proc replacement) body ...)
  "Within BODY, replace the definition of PROC from MODULE with the definition
given by REPLACEMENT."
  (let* ((m (resolve-module 'module))
         (original (module-ref m 'proc)))
    (dynamic-wind
      (lambda () (module-set! m 'proc replacement))
      (lambda () body ...)
      (lambda () (module-set! m 'proc original)))))

(test-begin "debbugs")

(define data-dir
  (string-append (getenv "abs_top_srcdir") "/tests/data"))

(test-equal "bug-id->summary-file: return archive summary file"
  (format #f "~a/spool/archive/95/26095.summary" data-dir)
  (mock ((mumi config) %config
         (match-lambda
           ('data-dir data-dir)))
        ((@@ (mumi debbugs) bug-id->summary-file) 26095)))

(test-equal "bug-id->summary-file: return active summary file"
  (format #f "~a/spool/db-h/99/33299.summary" data-dir)
  (mock ((mumi config) %config
         (match-lambda
           ('data-dir data-dir)))
        ((@@ (mumi debbugs) bug-id->summary-file) 33299)))

(test-end "debbugs")
